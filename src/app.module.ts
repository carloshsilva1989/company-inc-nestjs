import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { ClientsModule } from './clients/clients.module';
import { WalletsModule } from './wallets/wallets.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/companyinc'),
    AuthModule, 
    ClientsModule, WalletsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
