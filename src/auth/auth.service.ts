import { HttpException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { hash, compare } from 'bcrypt';
import { Model } from 'mongoose';
import { Client, ClientDocument } from 'src/clients/schema/client.schema';
import { LoginAuthDto } from './dto/login-auth-.dto';
import { RegisterAuthDto } from './dto/register-auth.dto';

@Injectable()
export class AuthService {
    constructor(@InjectModel(Client.name) private readonly clientModel: Model<ClientDocument>, private jwtService: JwtService){}

    async resgiter(registerAuthDto:RegisterAuthDto){
        //verify client exist
        const clientExist = await this.clientModel.exists({email:registerAuthDto.email})
        if(clientExist) throw new HttpException("Client exists", 400)
        //hash password
        registerAuthDto.password = await hash(registerAuthDto.password, 10)
        //register client
        return await this.clientModel.create(registerAuthDto)
    }

    async login(loginAuthDto:LoginAuthDto) {
        const client = await this.clientModel.findOne({email: loginAuthDto.email})
        //verify exists account
        if(!client) throw new HttpException("Client not found", 404);   
        //check password
        const checkPassword = await compare(loginAuthDto.password, client.password)
        if(!checkPassword) throw new HttpException("Password incorrect", 403)

        const data = {
            user: client,
            token: this.jwtService.sign({id: client._id, name: client.name})
        }
        
        return data
    }
}
