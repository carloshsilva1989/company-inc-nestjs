import { Body, Controller, Get, Post, Request, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { LoginAuthDto } from './dto/login-auth-.dto';
import { RegisterAuthDto } from './dto/register-auth.dto';
import { JwtAuthGuard } from './jwt-auth.guard';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('register')
  register(@Body() registerAuthDto:RegisterAuthDto){
    return this.authService.resgiter(registerAuthDto)
  }

  @Post('login')
  login (@Body() loginAuthDto:LoginAuthDto) {
    return this.authService.login(loginAuthDto)
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Get('userData')
  userData(@Request() req) {
    return req.user
  }
}
