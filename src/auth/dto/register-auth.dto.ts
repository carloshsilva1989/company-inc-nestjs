import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsInt, IsNotEmpty, MinLength, MaxLength, IsEmail } from 'class-validator';

export class RegisterAuthDto {
    @ApiProperty()
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @ApiProperty()
    @MinLength(4)
    @MaxLength(12)
    password: string;

    @ApiProperty()
    @IsString()
    name: string;

    @ApiProperty()
    @IsInt()
    gov_id: number
 
    @ApiProperty()
    @IsString()
    address: string
}