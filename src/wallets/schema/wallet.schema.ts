import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type WalletDocument = Wallet & Document;

//Todo el schema de los Wallet ( Collection )
@Schema()
export class Wallet {
    @Prop()
    user_id: string;

    @Prop()
    balance_USD: string;

    @Prop()
    balance_COP: string;
}

export const WalletSchema = SchemaFactory.createForClass(Wallet);