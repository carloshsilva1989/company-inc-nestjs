import { HttpException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateWalletDto } from './dto/create-wallet.dto';
import { UpdateWalletDto } from './dto/update-wallet.dto';
import { Wallet, WalletDocument } from './schema/wallet.schema';

@Injectable()
export class WalletsService {
  
  constructor(@InjectModel(Wallet.name) private readonly walletModel: Model<WalletDocument>){}

  async create(user_id, createWalletDto: CreateWalletDto) {
    const walletExist = await this.walletModel.findOne({user_id: user_id})
    if(!walletExist) {
      const data = {...createWalletDto, user_id:user_id}
      return await this.walletModel.create(data)
    }

    return walletExist
  }

  async update(user_id, updateWalletDto: UpdateWalletDto) {
    const walletExist = await this.walletModel.findOne({user_id: user_id})
    if(!walletExist) throw new HttpException("Wallet not found", 404);   
    const filter = { user_id: user_id};
    const update = { 
      balance_COP: String(updateWalletDto.balance_COP + Number(walletExist.balance_COP)),
      balance_USD: String(updateWalletDto.balance_USD + Number(walletExist.balance_USD))
    };

    // `doc` is the document _before_ `update` was applied
    let doc = await this.walletModel.findOneAndUpdate(filter, update);

    doc = await this.walletModel.findOne(filter);
    return new HttpException("Wallet updated", 200);  
  }

}
