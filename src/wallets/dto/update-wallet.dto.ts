import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateWalletDto } from './create-wallet.dto';
import { IsNotEmpty, IsEmail } from 'class-validator';

export class UpdateWalletDto {
    @ApiProperty()
    @IsNotEmpty()
    balance_USD: number;

    @ApiProperty()
    @IsNotEmpty()
    balance_COP: number;
}