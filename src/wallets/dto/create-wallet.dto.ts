import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class CreateWalletDto {
    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    balance_USD: number;

    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    balance_COP: number;
}