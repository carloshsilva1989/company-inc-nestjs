import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ClientDocument = Client & Document;

//Todo el schema de los Client ( Collection )
@Schema()
export class Client {
    @Prop()
    email: string;

    @Prop()
    password: string;

    @Prop()
    name: string;

    @Prop()
    gov_id: number
 
    @Prop()
    address: string
}

export const ClientSchema = SchemaFactory.createForClass(Client);