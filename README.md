## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Documentation Swagger
Enter into url http://localhost:3000/documentation#/ then register and then log in, enter the token in authorize, and execute the wallet creation and update apis.


